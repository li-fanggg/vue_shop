import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

//导入css
import "./assets/css/global.css"

//导入axios
import axios from "axios"

//设置一下默认根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'

//在把axios挂载到Vue原型上之前，需要通过 axios 请求拦截器添加 token ，保证拥有获取数据的权限
axios.interceptors.request.use((config) => {
  //为请求头对象,添加token 验证的Authorization 字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  //请求拦截器的固定写法  必须在最后return config
  return config
})

//把axios挂载到原型对象上
Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
