import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/components/Login.vue"

import Home from "@/components/Home.vue"

import Aside from "@/components/Aside.vue"
import Welcome from "@/components/Welcome.vue"
import User from "@/User/User.vue"

Vue.use(VueRouter)

const routes = [
  //开启路由重定向
  { path: "/", redirect: "/login" },
  { path: "/login", component: Login },
  {
    path: "/home", component: Home,
    redirect: "/welcome",
    children: [{ path: "/welcome", component: Welcome },
    { path: "/users", component: User }]
  },
  { path: '/userList', component: Aside }
]

const router = new VueRouter({
  routes
})

//挂载一个路由守卫
router.beforeEach((to, from, next) => {
  //如果需要去往登录页，则直接放行
  if (to.path === '/login') return next()
  //拿到sessionstorage.token的值
  const token = window.sessionStorage.getItem('token')
  //如果token的值存在  则放行到其他页面  如果不存在 则跳转到登录页
  if (!token) return next('/login')
  next()
})

export default router
